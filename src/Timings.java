
public class Timings extends Thread
{
	
	private int convertAmnt = 1;

	private Graphic gr;
	private AutoGen gen;
	private LifetimeVal val;
	
	public void Starter()
	{
		
		gr = new Graphic();
		gen = new AutoGen();
		val = new LifetimeVal();
		
		Thread time = new Thread(this);
		time.start();
		
	}
	
	public void run()
	{
		
		while (true)
		{
				
			convertAmnt = gen.getTruckAmnt() + 1;
			
			if (gr.getGummyWormAmnt() >= convertAmnt)
			{
			
				gr.setGummyWormAmnt(gr.getGummyWormAmnt() - convertAmnt);
				gr.setMoneyAmnt(gr.getMoneyAmnt() + convertAmnt);
				val.setLifetimeM(val.getLifetimeM() + convertAmnt);
				
			}
			
			try
			{
				
				Thread.sleep((int)((gen.getTimeAmnt() * 1000)));
				
			} 
			catch (InterruptedException e) 
			{
				
				e.printStackTrace();
				
			}
			
		}
		
	}

	public int getConvertAmnt() {
		return convertAmnt;
	}

	public void setConvertAmnt(int convertAmnt) {
		this.convertAmnt = convertAmnt;
	}
	
}
