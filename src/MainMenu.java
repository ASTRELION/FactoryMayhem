import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class MainMenu extends JFrame implements MouseListener
{

	private Text playButton;
	private JLabel warning;

	public MainMenu()
	{
		
		super("Main Menu");
		
		playButton = new Text();
		
		addMouseListener(this);
		
		setSize(500, 400);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		
		playButton.setSize(getSize());
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2); //centering window
		
		add(playButton, BorderLayout.CENTER);
		
	}

	//Mouse Clicked Event
	public void mouseClicked(MouseEvent e) 
	{
		
		//Play Button
		if (e.getX() > 185 && e.getX() < 325) //<left-most> <right most>
		{
			
			if (e.getY() < 160 && e.getY() > 95) //<down-most> <up-most>
			{
				
				dispose();
				System.out.println("Setting up game...");
				GameWindow g = new GameWindow(); //runs game
				
				g.Starter();
				
			}
			
		}
		
		//Options
		if (e.getX() > 210 && e.getX() < 280) //<left-most> <right most>
		{
			
			if (e.getY() < 215 && e.getY() > 200) //<down-most> <up-most>
			{
				
				Menus m = new Menus();
				m.Options();
				
			}
			
		}
		
		//Change Log
		if (e.getX() > 165 && e.getX() < 330) //<left-most> <right most>
		{
			
			if (e.getY() < 255 && e.getY() > 240) //<down-most> <up-most>
			{
				
				Menus m = new Menus();
				m.Changes();
				
			}
			
		}
		
		//Credits
		if (e.getX() > 210 && e.getX() < 275) //<left-most> <right most>
		{
			
			if (e.getY() < 295 && e.getY() > 280) //<down-most> <up-most>
			{
				
				Menus m = new Menus();
				m.Credits();
				
			}
			
		}
		
		//QuitGame Button
		if (e.getX() > 200 && e.getX() < 290) //<left-most> <right most>
		{
			
			if (e.getY() < 335 && e.getY() > 320) //<down-most> <up-most>
			{
				
				setVisible(false);
				
				try
				{
					
					dispose();
					
				}
				catch(Exception ex)
				{
					
					System.out.println("Could not quit game");
					ex.printStackTrace();
					System.out.println("Could not quit game");
					
				}

				System.out.println("Game Quit");
				
			}
			
		}
		
	}

	public void mouseEntered(MouseEvent e) 
	{
		
		//none
		
	}

	public void mouseExited(MouseEvent e) 
	{
		
		//none
		
	}

	public void mousePressed(MouseEvent e) 
	{
		
		//none
		
	}

	public void mouseReleased(MouseEvent e)
	{
		
		//none
		
	}
	
}
