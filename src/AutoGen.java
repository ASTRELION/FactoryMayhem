
public class AutoGen extends Thread
{
	
	private static int truckAmnt = 0, guardAmnt = 0, salesAmnt = 0;
	
	private static int wWorkerAmnt = 0, bWorkerAmnt = 0, gWorkerAmnt = 0, rWorkerAmnt = 0;
	private double wWorkerGenAmnt = 1; //Worst
	private double bWorkerGenAmnt = 3;
	private double gWorkerGenAmnt = 10;
	private double rWorkerGenAmnt = 20; //Best
	
	private double tempGummyWormAmnt = 0;
	private static double timeAmnt = 5;
	
	private LifetimeVal val;
	private Graphic gr;

	public void Starter()
	{
		
		gr = new Graphic();
		
		Thread gen = new Thread(this);
		gen.start();
		
	}
	
	public void run()
	{
		
		while (true)
		{
		
			if(timeAmnt >= 1)
			{
				
				timeAmnt = 5 - salesAmnt;
				
			}
			
			if (timeAmnt < 1)
			{
				
				timeAmnt = Math.ceil((5 / salesAmnt));
				
			}
			
			if(timeAmnt == 0)
			{
				
				timeAmnt = 1;
				
			}
			
			tempGummyWormAmnt += (wWorkerGenAmnt * wWorkerAmnt) / 10;
			tempGummyWormAmnt += (bWorkerGenAmnt * bWorkerAmnt) / 10;
			tempGummyWormAmnt += (gWorkerGenAmnt * gWorkerAmnt) / 10;
			tempGummyWormAmnt += (rWorkerGenAmnt * rWorkerAmnt) / 10;
			gr.setGummyWormAmnt(gr.getGummyWormAmnt() + (int)Math.floor(tempGummyWormAmnt));
			
			val.setLifetimeGW(val.getLifetimeGW() + (int)Math.floor(tempGummyWormAmnt));
			
			if (Math.floor(tempGummyWormAmnt) > 0)
			{
				
				tempGummyWormAmnt = 0;
				
			}
			
			try
			{
				
				Thread.sleep(100);
				
			}
			catch (Exception e)
			{
				
				e.printStackTrace();
				
			}
			
		}
		
	}

	public int getWWorkerAmnt() {
		return wWorkerAmnt;
	}

	public void setWWorkerAmnt(int wWorkerAmnt) {
		this.wWorkerAmnt = wWorkerAmnt;
	}

	public int getTruckAmnt() {
		return truckAmnt;
	}

	public void setTruckAmnt(int truckAmnt) {
		this.truckAmnt = truckAmnt;
	}

	public int getGuardAmnt() {
		return guardAmnt;
	}

	public void setGuardAmnt(int guardAmnt) {
		this.guardAmnt = guardAmnt;
	}

	public static int getSalesAmnt() {
		return salesAmnt;
	}

	public static void setSalesAmnt(int salesAmnt) {
		AutoGen.salesAmnt = salesAmnt;
	}

	public double getTimeAmnt() {
		return timeAmnt;
	}

	public void setTimeAmnt(int timeAmnt) {
		this.timeAmnt = timeAmnt;
	}

	public static int getbWorkerAmnt() {
		return bWorkerAmnt;
	}

	public static void setbWorkerAmnt(int bWorkerAmnt) {
		AutoGen.bWorkerAmnt = bWorkerAmnt;
	}

	public static int getgWorkerAmnt() {
		return gWorkerAmnt;
	}

	public static void setgWorkerAmnt(int gWorkerAmnt) {
		AutoGen.gWorkerAmnt = gWorkerAmnt;
	}

	public static int getrWorkerAmnt() {
		return rWorkerAmnt;
	}

	public static void setrWorkerAmnt(int rWorkerAmnt) {
		AutoGen.rWorkerAmnt = rWorkerAmnt;
	}
	
}
