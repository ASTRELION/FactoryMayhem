
public class TimePlayed extends Thread
{

	private static int day = 0, hour = 0, minute = 0, second = 0;
	
	public void Starter()
	{
		
		Thread t = new Thread(this);
		t.start();
		
	}
	
	public void run()
	{
		
		while (true)
		{
			
			if (second < 59)
			{
				
				second++;
				
			}
			else if (second >= 59)
			{
				
				second = 0;
				minute++;
				
			}
			
			if (minute >= 60)
			{
				
				minute = 0;
				hour++;
				
			}
			
			if (hour >= 24)
			{
				
				hour = 0;
				day++;
				
			}
			
			try
			{
				
				Thread.sleep(1000);
				
			}
			catch (Exception e)
			{
				
				e.printStackTrace();
				
			}
		
		}
		
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		this.second = second;
	}
	
}
