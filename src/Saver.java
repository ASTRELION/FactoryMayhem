import java.io.File;
import java.util.Formatter;
import java.util.Scanner;

public class Saver 
{

	private static Formatter form;
	public static Scanner reader;
	public static int GW, M, wW, bW, gW, rW, S, T, G;

	public static void CreateFile()
	{
	
		try
		{
			
			form = new Formatter("FactoryMayhemSave.txt");
			System.out.println("Created new save file.");
			
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
			
		}
		
	}
	
	public static void OpenFile()
	{
		
		try
		{
		
			reader = new Scanner(new File("FactoryMayhemSave.txt"));
		
		}
		catch(Exception e)
		{
			
			e.printStackTrace();
			
		}
		
	}
	
	public static void ReadFile()
	{
		
		AutoGen gen = new AutoGen();
		Graphic gr = new Graphic();
		
		while(reader.hasNext())
		{
			
			String GmmyWrms = reader.next();
			String Mny = reader.next();
			String wWrkr = reader.next();
			String bWrkr = reader.next();
			String gWrkr = reader.next();
			String rWrkr = reader.next();
			String SlsMn = reader.next();
			String Trck = reader.next();
			String Grd = reader.next();
			
			GW = Integer.parseInt(GmmyWrms);
			M = Integer.parseInt(GmmyWrms);
			wW = Integer.parseInt(GmmyWrms);
			bW = Integer.parseInt(GmmyWrms);
			gW = Integer.parseInt(GmmyWrms);
			rW = Integer.parseInt(GmmyWrms);
			S = Integer.parseInt(GmmyWrms);
			T = Integer.parseInt(GmmyWrms);
			G = Integer.parseInt(GmmyWrms);
			
			gr.setGummyWormAmnt(GW);
			gr.setMoneyAmnt(M);
			gen.setWWorkerAmnt(wW);
			gen.setbWorkerAmnt(bW);
			gen.setgWorkerAmnt(gW);
			gen.setrWorkerAmnt(rW);
			gen.setSalesAmnt(S);
			gen.setTruckAmnt(T);
			gen.setGuardAmnt(G);
			
		}
		
	}
	
	public static void AddRecords()
	{
		
		AutoGen gen = new AutoGen();
		Graphic gr = new Graphic();
		
		form.format("%o %o %o %o %o %o %o %o %o", gr.getGummyWormAmnt(), gr.getMoneyAmnt(), gen.getWWorkerAmnt(), AutoGen.getbWorkerAmnt(), AutoGen.getgWorkerAmnt(), AutoGen.getrWorkerAmnt(), AutoGen.getSalesAmnt(), gen.getTruckAmnt(), gen.getGuardAmnt());
		
	}
	
	public static void CloseFile()
	{
		
		form.close();
		
	}
	
}
