import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFrame;

public class GameWindow extends JFrame implements MouseListener, Runnable
{

	private Graphic gr;
	private Timings time;
	private Menus m;
	private AutoGen gen;
	private TimePlayed played;
	private LifetimeVal lV;
	private Saver save;
	
	private static int wWorkerCost = 5, bWorkerCost = 25, gWorkerCost = 50, rWorkerCost = 100;
	private static int salesManCost = 50;
	private static int truckCost = 30;
	
	//Start game method
	public void Starter()
	{
		
		time = new Timings();
		time.Starter();
		gen = new AutoGen();
		gen.Starter();
		gr = new Graphic();
		gr.Starter();
		played = new TimePlayed();
		played.Starter();
		lV = new LifetimeVal();
		m = new Menus();
		save = new Saver();
		
		Thread t = new Thread(this);
		t.start();
		
		addMouseListener(this);
		
		setSize(1000, 600);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBackground(Color.CYAN);
		
		URL url = getClass().getResource("/Images/Background.png");
		Toolkit kit = Toolkit.getDefaultToolkit();
		Image img = kit.createImage(url);
		setIconImage(img);

		gr.setSize(getSize());
		add(gr);

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2); //centering window
		
		/*try
		{
			
			Saver.OpenFile();
			m.SaveDetected();
			Saver.CloseFile();
			
		}
		catch(Exception e)
		{
			
			Saver.CreateFile();
			Saver.CloseFile();
			
		}*/
		
	}
	
	//Updater
	public void run()
	{
		
		while (true)
		{
			
			repaint();
			
			//Saver.OpenFile();
			//Saver.AddRecords();
			//Saver.CloseFile();
			
			if (gr.getGummyWormAmnt() < time.getConvertAmnt())
			{
				
				gr.setOut(true);
				
			}
			else
			{
				
				gr.setOut(false);
				
			}
			
			try
			{
				
				Thread.sleep(17);
				
			}
			catch(Exception e)
			{
				
				e.printStackTrace();
				
			}
			
		}
		
	}

	public void mouseClicked(MouseEvent e) 
	{
		
		//System.out.println("x: " + e.getX() + "\ny: " + e.getY());
		
		//BUY
		
		//WORKERS
		
		//WHITE WORKER
		if (e.getX() < 770 && e.getX() >= 740) //Rightmost, leftmost
		{
			
			if (e.getY() < 75 && e.getY() > 50) //downmost, upmost
			{
				
				if (gr.getMoneyAmnt() >= wWorkerCost)
				{
					
					gen.setWWorkerAmnt(gen.getWWorkerAmnt() + 1);
					gr.setMoneyAmnt(gr.getMoneyAmnt() - wWorkerCost);
					
					wWorkerCost = wWorkerCost + (gen.getWWorkerAmnt() * 5);
					
				}
				
			}
			
		}
		
		//BLUE WORKER
		if (e.getX() < 805 && e.getX() >= 775) //Rightmost, leftmost
		{
			
			if (e.getY() < 75 && e.getY() > 50) //downmost, upmost
			{
				
				if (gr.getMoneyAmnt() >= bWorkerCost)
				{
					
					gen.setbWorkerAmnt(gen.getbWorkerAmnt() + 1);
					gr.setMoneyAmnt(gr.getMoneyAmnt() - bWorkerCost);
					
					bWorkerCost = bWorkerCost + (gen.getbWorkerAmnt() * 15);
					
				}
				
			}
			
		}
		
		//GREEN WORKER
		if (e.getX() < 770 && e.getX() >= 740) //Rightmost, leftmost
		{
			
			if (e.getY() < 115 && e.getY() > 85) //downmost, upmost
			{
				
				if (gr.getMoneyAmnt() >= gWorkerCost)
				{
					
					gen.setgWorkerAmnt(gen.getgWorkerAmnt() + 1);
					gr.setMoneyAmnt(gr.getMoneyAmnt() - gWorkerCost);
					
					
					gWorkerCost = gWorkerCost + (gen.getgWorkerAmnt() * 20);
					
				}
				
			}
			
		}
		
		//RED WORKER
		if (e.getX() < 805 && e.getX() >= 775) //Rightmost, leftmost
		{
			
			if (e.getY() < 115 && e.getY() > 85) //downmost, upmost
			{
				
				if (gr.getMoneyAmnt() >= rWorkerCost)
				{
					
					gen.setrWorkerAmnt(gen.getrWorkerAmnt() + 1);
					gr.setMoneyAmnt(gr.getMoneyAmnt() - rWorkerCost);
					
					rWorkerCost = rWorkerCost + (gen.getrWorkerAmnt() * 25);
					
				}

			}
			
		}
		
		//SALES MAN
		if (e.getX() < 955 && e.getX() >= 915) //Rightmost, leftmost
		{
			
			if (e.getY() < 105 && e.getY() > 50) //downmost, upmost
			{
				
				if(gr.getMoneyAmnt() >= salesManCost && gen.getSalesAmnt() < 5)
				{
					
					gen.setSalesAmnt(gen.getSalesAmnt() + 1);
					gr.setMoneyAmnt(gr.getMoneyAmnt() - salesManCost);
					
					salesManCost = salesManCost + (gen.getSalesAmnt() * 30);
					
				}
				
			}
			
		}
		
		//TRUCK
		if (e.getX() < 790 && e.getX() >= 750) //Rightmost, leftmost
		{
			
			if (e.getY() < 210 && e.getY() > 150) //downmost, upmost
			{
				
				if (gr.getMoneyAmnt() >= truckCost)
				{
					
					gen.setTruckAmnt(gen.getTruckAmnt() + 1);
					gr.setMoneyAmnt(gr.getMoneyAmnt() - truckCost);
					
					truckCost = truckCost + (gen.getTruckAmnt() * 17);
					
				}
				
			}
			
		}
		
		//GUARD
		if (e.getX() < 940 && e.getX() >= 900) //Rightmost, leftmost
		{
			
			if (e.getY() < 210 && e.getY() > 155) //downmost, upmost
			{
				
				gen.setGuardAmnt(gen.getGuardAmnt() + 1);
				
			}
			
		}
				
		//OPTION BUTTONS
		//Manual gummy worm gen area
		if (e.getX() < 420 && e.getX() >= 0) //Rightmost, leftmost
		{
			
			if (e.getY() < 535 && e.getY() > 190) //downmost, upmost
			{
				
				gr.setGummyWormAmnt(gr.getGummyWormAmnt() + 1);
				lV.setLifetimeGW(lV.getLifetimeGW() + 1);
				gr.setClicked(true);
				
			}
			
		}
		
		//Options
		if (e.getX() < 80 && e.getX() >= 12) //Rightmost, leftmost
		{
			
			if (e.getY() < 585 && e.getY() > 570) //downmost, upmost
			{
				
				m.Options();
				
			}
			
		}
		
		//Stats
		if (e.getX() < 230 && e.getX() >= 190) //Rightmost, leftmost
		{
			
			if (e.getY() < 585 && e.getY() > 570) //downmost, upmost
			{
				
				Menus m = new Menus();
				m.Stats();
				
			}
			
		}
		
		//Console
		if (e.getX() < 170 && e.getX() >= 100) //Rightmost, leftmost
		{
			
			if (e.getY() < 585 && e.getY() > 570) //downmost, upmost
			{
				
				Menus menu = new Menus();
				menu.Console();
				
			}
			
		}
		
		//Save
		if (e.getX() < 340 && e.getX() >= 300) //Rightmost, leftmost
		{
			
			if (e.getY() < 585 && e.getY() > 570) //downmost, upmost
			{
				
				System.out.println("No saving yet");
				
			}
			
		}
		
		//Exit
		if (e.getX() < 585 && e.getX() >= 360) //Rightmost, leftmost
		{
			
			if (e.getY() < 585 && e.getY() > 570) //downmost, upmost
			{
				
				dispose();
				
			}
			
		}
		
	}

	public int getSalesManCost() {
		return salesManCost;
	}

	public void setSalesManCost(int salesManCost) {
		this.salesManCost = salesManCost;
	}

	public int getTruckCost() {
		return truckCost;
	}

	public void setTruckCost(int truckCost) {
		this.truckCost = truckCost;
	}

	public void mouseEntered(MouseEvent e) 
	{
	
		//none
		
	}

	public void mouseExited(MouseEvent e) 
	{
		
		//none
		
	}

	public void mousePressed(MouseEvent e) 
	{
	
		//none
		
	}

	public void mouseReleased(MouseEvent e) 
	{
		
		//none
		
	}

	public static int getwWorkerCost() {
		return wWorkerCost;
	}

	public static void setwWorkerCost(int wWorkerCost) {
		GameWindow.wWorkerCost = wWorkerCost;
	}

	public static int getbWorkerCost() {
		return bWorkerCost;
	}

	public static void setbWorkerCost(int bWorkerCost) {
		GameWindow.bWorkerCost = bWorkerCost;
	}

	public static int getgWorkerCost() {
		return gWorkerCost;
	}

	public static void setgWorkerCost(int gWorkerCost) {
		GameWindow.gWorkerCost = gWorkerCost;
	}

	public static int getrWorkerCost() {
		return rWorkerCost;
	}

	public static void setrWorkerCost(int rWorkerCost) {
		GameWindow.rWorkerCost = rWorkerCost;
	}
	
}
