import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Graphic extends JPanel
{
	
	private static int gummyWormAmnt = 0;
	private static int moneyAmnt = 0;
	private static boolean out, clicked = false;
	
	private AutoGen gen;
	private GameWindow gw;
	
	private URL url;
	
	public void Starter()
	{
		
		gen = new AutoGen();
		gw = new GameWindow();
		
	}

	public void paintComponent(Graphics g)
	{
		
		super.paintComponent(g);
			
		//Background image
		url = getClass().getResource("/Images/Background.png"); //Getting Image
		Image screen = null;
			
		try 
		{
				
			screen = ImageIO.read(url); //Reading image
				
		} 
		catch (IOException e) 
		{
				
			e.printStackTrace();
				
		}
	
		g.drawImage(screen, 0, 0, this); //Painting imag
			
		//Variable labels
		Font f = new Font("Serif", Font.BOLD, 20);
		g.setFont(f);
		g.setColor(Color.BLUE);
		g.drawString("Gummy Worms:", 10, 40);
		g.drawString("Money:", 10, 65);
			
		//Amount of variables
		String gMAS = Integer.toString(gummyWormAmnt);
		String mAS = Integer.toString(moneyAmnt);
				
		g.setColor(Color.WHITE);
		g.drawString(gMAS.toString(), 170, 41); //Amount of gummy worms
		g.drawString(mAS, 90, 66); //amount of money
		
		if (!clicked)
		{
			
			Font click = new Font("Serif", Font.BOLD, 25);
			g.setFont(click);
			g.setColor(Color.WHITE);
			g.drawString("Click factory to get gummy worms!", 450, 300);
			
		}
		
		//Out of worms alert
		if (out)
		{
			
			Font out = new Font("Serif", Font.BOLD, 13);
			g.setFont(out);
			g.setColor(Color.RED);
			g.drawString("Out of Gummy Worms to sell, make more!", 10, 85);
			
		}
		
		//**IN GAME GRAPHICS**
		
		Font costs = new Font("Serif", Font.PLAIN, 12);
		Font labels = new Font("Serif", Font.ITALIC, 15);
		g.setFont(labels);
		g.setColor(Color.WHITE);
		g.drawString("Worker", 750, 20); //Label
		
			g.setColor(Color.WHITE);
			g.fillRect(740, 25, 30, 30); //White worker
			g.setColor(Color.BLACK);
			String paintWWorker = Integer.toString(gen.getWWorkerAmnt()); //White amount
			g.drawString(paintWWorker, 720, 35);
			
			g.setFont(costs);
			String wWorkerC = Integer.toString(gw.getwWorkerCost());
			g.drawString("$" + wWorkerC, 740, 45);
			
			g.setFont(labels);
			g.setColor(Color.BLUE);
			g.fillRect(775, 25, 30, 30); //Blue worker
			g.setColor(Color.BLACK);
			String paintBWorker = Integer.toString(gen.getbWorkerAmnt()); //Blue amount
			g.drawString(paintBWorker, 810, 35);
			
			g.setFont(costs);
			String bWorkerC = Integer.toString(gw.getbWorkerCost());
			g.drawString("$" + bWorkerC, 775, 45);
			
			g.setFont(labels);
			g.setColor(Color.GREEN);
			g.fillRect(740, 60, 30, 30); //Green worker
			g.setColor(Color.BLACK);
			String paintGWorker = Integer.toString(gen.getgWorkerAmnt()); //Green amount
			g.drawString(paintGWorker, 720, 70);
			
			g.setFont(costs);
			String gWorkerC = Integer.toString(gw.getgWorkerCost());
			g.drawString("$" + gWorkerC, 740, 80);
			
			g.setFont(labels);
			g.setColor(Color.RED);
			g.fillRect(775, 60, 30, 30); //Red worker
			g.setColor(Color.BLACK);
			String paintRWorker = Integer.toString(gen.getrWorkerAmnt()); //Red amount
			g.drawString(paintRWorker, 810, 70);
			
			g.setFont(costs);
			String rWorkerC = Integer.toString(gw.getrWorkerCost());
			g.drawString("$" + rWorkerC, 775, 80);
		
		g.setFont(labels);
		g.setColor(Color.WHITE);
		g.drawString("Sales-man", 900, 20); //Label
		g.fillRect(915, 25, 40, 60);
		g.setColor(Color.BLACK);
		String paintSales = Integer.toString(gen.getSalesAmnt());
		g.drawString(paintSales, 895, 35);
		
			g.setFont(costs);
			String salesC = Integer.toString(gw.getSalesManCost());
			
			if (gen.getSalesAmnt() >= 5)
			{
				
				salesC = "";
				g.drawString(salesC, 915, 60);
				
			}
			else
			{
				
				g.drawString("$" + salesC, 915, 60);
				
			}

		g.setFont(labels);
		g.setColor(Color.WHITE);
		g.drawString("Truck", 750, 125); //Label
		g.fillRect(750, 130, 40, 60);
		g.setColor(Color.BLACK);
		String paintTruck = Integer.toString(gen.getTruckAmnt());
		g.drawString(paintTruck, 730, 140);
		
			g.setFont(costs);
			String truckC = Integer.toString(gw.getTruckCost());
			g.drawString("$" + truckC, 750, 165);
		
		g.setFont(labels);
		g.setColor(Color.WHITE);
		g.drawString("Guard", 900, 125); //Label
		g.fillRect(900, 130, 40, 60);
		g.setColor(Color.BLACK);
		String paintGuard = Integer.toString(gen.getGuardAmnt());
		g.drawString(paintGuard, 880, 140);
		
		//********************
		
		g.setColor(Color.BLACK);
		g.fillRect(6, 540, 400, 30);
		
		g.setColor(Color.WHITE);
		g.setFont(f);
		
		//In-game Options
		g.drawString("Options", 10, 562);
		
		g.drawString("Console", 100, 562);
		
		g.setColor(Color.YELLOW);
		g.drawString("Stats", 190, 562);
		
		g.setColor(Color.GREEN);
		g.drawString("Save", 300, 562);
		
		g.setColor(Color.RED);
		g.drawString("Exit", 360, 562);
		
	}
	
	public int getGummyWormAmnt() 
	{
		
		return gummyWormAmnt;
		
	}

	public void setGummyWormAmnt(int gummyWormAmnt) 
	{
		
		this.gummyWormAmnt = gummyWormAmnt;
		
	}

	public int getMoneyAmnt() 
	{
		
		return moneyAmnt;
		
	}

	public void setMoneyAmnt(int moneyAmnt) 
	{
		
		this.moneyAmnt = moneyAmnt;
		
	}
	
	public void setOut(boolean out) {
		this.out = out;
	}

	public static boolean isClicked() {
		return clicked;
	}

	public static void setClicked(boolean clicked) {
		Graphic.clicked = clicked;
	}
	
}
