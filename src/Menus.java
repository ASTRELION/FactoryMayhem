import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Menus extends JFrame
{
	
	private Graphic gr;
	private TimePlayed ply;
	private LifetimeVal val;
	private Saver save;
	
	private JFrame saveD;

	//Options
	public void Options()
	{
		
		JFrame options = new JFrame();
		options.setLayout(new FlowLayout());
		options.setVisible(true);
		options.setResizable(false);
		options.setSize(300,300);
		options.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		options.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
	}
	
	//Changes
	public void Changes()
	{
		
		JFrame changes = new JFrame();
		changes.setTitle("Planned Features and Change-Log");
		changes.setLayout(new FlowLayout());
		changes.setVisible(true);
		changes.setResizable(false);
		changes.setSize(450,400);
		changes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		changes.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
		JTextArea text = new JTextArea(23, 34);
		text.setEditable(false);
		text.setText("Planned Features:"
				+ "\n"
				+ "\n-Saving"
				+ "\n-ALMOST-Stats"
				+ "\n-Options menu"
				+ "\n-Employ different types of workers"
				+ "\n-DONE-Add gummy worm to money conversion"
				+ "\n-Total amounts of gummy worms and money earned"
				+ "\n-DONE-Upgradeable shipment trucks"
				+ "\n-Enemies (Gummy Bears?) that attack"
				+ "\n-Purchasable guards"
				+ "\n-DONE-Sales men to boost sale time"
				+ "\n\n"
				+ "Change Log: "
				+ "\n"
				+ "\n0.0.7"
				+ "\n+Added Blue, Green, and Red workers! (B = 3GpS, G = 10GpS, R = 20GpS)"
				+ "\n+Added price label to each upgrade"
				+ "\n*Changed layout slightly"
				+ "\n"
				+ "\n0.0.6"
				+ "\n+Added lifetime money and lifetime gummy worms"
				+ "\n*Fixed other small bugs"
				+ "\n"
				+ "\n0.0.5"
				+ "\n+Shipment Trucks finished (Each adds 1 to GW to M amount)"
				+ "\n+Added stats button"
				+ "\n+Stats currently display only current gummy worm and money amount and total time"
				+ "\n*Fixed time being slightly off"
				+ "\n*Fixed workers not making any GW"
				+ "\n*Small bug fixes"
				+ "\n"
				+ "\n0.0.4"
				+ "\n+Basic workers finished (Every worker earns add 1 to GwpS)"
				+ "\n+Sales men finished, buggy (Every sales men reduces GW to M time) (Max of 5)"
				+ "\n**Better graphics coming soon"
				+ "\n*Small bug fixes"
				+ "\n*Changed default sell time to 5 seconds"
				+ "\n"
				+ "\nv0.0.3"
				+ "\n+Warning when no GW are being sold"
				+ "\n+Begun progress on console"
				+ "\n+Beginning of the game click prompt"
				+ "\n"
				+ "\nv0.0.2"
				+ "\n+Ingame option buttons"
				+ "\n+Ingame exit"
				+ "\n+Gummy worm convert to money after time (default 1/6s)"
				+ "\n*Various small bug fixes"
				+ "\n"
				+ "\nv0.0.1"
				+ "\n+Main Menu"
				+ "\n+Options on main menu, play text"
				+ "\n+Added game window"
				+ "\n+Added this window ;)"
				+ "\n+Added manual GW making"
				+ "\n*Fixed major graphics flashing bug on Windows computers"
				+ "\n*Various small bug fixes");
		
		JScrollPane scroll = new JScrollPane(text);
		scroll.setBounds(0, 0, 100, 100);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		changes.add(scroll, BorderLayout.CENTER);
		
	}
	
	//Credits
	public void Credits()
	{
		
		JFrame credits = new JFrame();
		credits.setLayout(new FlowLayout());
		credits.setVisible(true);
		credits.setResizable(false);
		credits.setSize(300,300);
		credits.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		credits.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
		JLabel creds = new JLabel();
		creds.setText("<html>"
				+ "<br>Author: "
				+ "<br>ASTRELION"
				+ "<br>"
				+ "<br>Contributors: "
				+ "<br>"
				+ "<br>Lines of Code: ~1,215"
				+ "<html>");
		credits.add(creds, BorderLayout.CENTER);
		
	}
	
	//Console
	public void Console()
	{
		
		JFrame console = new JFrame();
		console.setLayout(new FlowLayout());
		console.setVisible(true);
		console.setResizable(false);
		console.setSize(700,400);
		console.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		console.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
		JTextArea text = new JTextArea(23, 56);
		text.setEditable(true);
		text.setText("CONSOLE - NOT FINISHED\n");
		
		JScrollPane window = new JScrollPane(text);
		window.setBounds(0, 0, 500, 500);
		window.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		console.add(window, BorderLayout.CENTER);
		
	}
	
	public void Stats()
	{

		gr = new Graphic();
		ply  = new TimePlayed();
		val = new LifetimeVal();
		
		Thread reload = new Thread()
		{
			
			public void run()
			{
				
				JFrame stats = new JFrame();
				stats.setLayout(new FlowLayout());
				stats.setVisible(true);
				stats.setResizable(true);
				stats.setSize(500,150);
				stats.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				
				JLabel window = new JLabel();
				
				while (true)
				{

					window.setText("<html>"
							+ "Factory Mayhem STATS<br><br>"
							+ "Lifetime Gummy Worms: " + val.getLifetimeGW()
							+ "<br>"
							+ "Lifetime Money: " + val.getLifetimeM()
							+ "<br>"
							+ "Gummy Worms Now: " + gr.getGummyWormAmnt()
							+ "<br>"
							+ "Money Now: " + gr.getMoneyAmnt()
							+ "<br>"
							+ "Time Played: " + ply.getDay() + " day(s), " + ply.getHour() + " hour(s), " + ply.getMinute() + " minute(s), " + ply.getSecond() + " second(s)."
							+ "<html>");
					
					stats.add(window, BorderLayout.CENTER);
				
				}
				
			}
			
		};
		
		reload.start();

	}
	
	public void SaveDetected()
	{
		
		saveD = new JFrame();
		saveD.setLayout(new FlowLayout());
		saveD.setVisible(true);
		saveD.setResizable(false);
		saveD.setSize(700,400);
		saveD.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		saveD.setLocation(dim.width/2-getSize().width/2, dim.height/2-getSize().height/2);
		
		save = new Saver();
		
		JLabel alert = new JLabel("Save file detected, what would you like to do?");
		JButton newS = new JButton("Start new game");
		JButton loadS = new JButton("Load save");
		
		saveD.add(alert, BorderLayout.CENTER);
		saveD.add(newS, BorderLayout.CENTER);
		saveD.add(loadS, BorderLayout.CENTER);
		
		newClass nC = new newClass();
		loadClass lC = new loadClass();
		
		newS.addActionListener(nC);
		loadS.addActionListener(lC);
		
	}
	
	private class newClass implements ActionListener
	{

		public void actionPerformed(ActionEvent e) 
		{
			
			Saver.CreateFile();
			Saver.CloseFile();
			saveD.dispose();
			
		}
		
	}
	
	private class loadClass implements ActionListener
	{

		public void actionPerformed(ActionEvent e) 
		{
			
			Saver.OpenFile();
			Saver.ReadFile();
			Saver.CloseFile();
			saveD.dispose();
			
		}
		
	}
	
}
