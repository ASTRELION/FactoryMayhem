import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Text extends JPanel
{

	public void paintComponent(Graphics g)
	{
		
		super.paintComponent(g);
		
		setBackground(getBackground());
		
		//Play Text
		g.setColor(new Color(7, 196, 13));
		Font playFont = new Font("Serif", Font.BOLD, 75);
		g.setFont(playFont);
		g.drawString("Play", 180, 125);
				
		//***OPTION BUTTONS***
		
		Font menuFont = new Font("Serif", Font.BOLD, 20);
		Font v = new Font("Serif", Font.BOLD, 15);
		
		//Options
		g.setColor(new Color(229, 120, 24));
		g.setFont(menuFont);
		g.drawString("Options", 210, 190);
		
		//Change Log
		g.setColor(new Color(229, 120, 24));
		g.setFont(menuFont);
		g.drawString("What's Happening", 165, 230);
		
		//Version Number
		g.setColor(Color.BLACK);
		g.setFont(v);
		g.drawString("v0.0.7", 5, 370);
		
		//Credits
		g.setColor(new Color(229, 120, 24));
		g.setFont(menuFont);
		g.drawString("Credits", 212, 270);
		
		//Quit
		g.setColor(Color.RED);
		g.setFont(menuFont);
		g.drawString("Quit Game", 195, 310);
		
	}
	
}
