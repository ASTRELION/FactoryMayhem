
public class LifetimeVal
{
	
	private static int lifetimeGW, lifetimeM;

	public static int getLifetimeGW() {
		return lifetimeGW;
	}

	public static void setLifetimeGW(int lifetimeGW) {
		LifetimeVal.lifetimeGW = lifetimeGW;
	}

	public static int getLifetimeM() {
		return lifetimeM;
	}

	public static void setLifetimeM(int lifetimeM) {
		LifetimeVal.lifetimeM = lifetimeM;
	}
	
}
